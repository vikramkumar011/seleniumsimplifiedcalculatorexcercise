package com.selenium.test.runner;



import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        dryRun = false,
        features = "src/test/resources/testFeatures",
        glue = {"com.selenium.web.stepDefinitions","com.selenium.test.steps"},
        tags = "@exercise",
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
)
public class TestRunner extends SetupTeardown{

}
