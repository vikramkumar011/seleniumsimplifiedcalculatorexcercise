package com.selenium.test.runner;


import com.selenium.test.steps.Hooks;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class SetupTeardown {
    public static final Logger log = LogManager.getLogger(Hooks.class);

    @BeforeClass
    public static void actionBefore(){
        log.info("Before Class");
    }

    @AfterClass
    public static void tearDown(){
        log.info("After Class");
    }
}
