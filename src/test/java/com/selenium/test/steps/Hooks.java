package com.selenium.test.steps;

import com.selenium.web.context.TestContext;
import io.cucumber.java.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;

public class Hooks {
    TestContext testContext;

    public Hooks(TestContext context){
        testContext = context;
    }
    public static final Logger log = LogManager.getLogger(Hooks.class);
    @Before
    public void beforeScenario(Scenario scenario){
        log.info("*******Logger Started******");
        log.info("           Started Scenario Started : "+scenario.getName());
    }
    @After
    public void afterScenario(Scenario scenario) throws IOException {
        log.info("           Scenario Ended : "+scenario.getName());
        testContext.getWebDriverManager().closeDriver();
        if(scenario.isFailed()){
            try {
                testContext.getWebDriverManager().closeDriver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
