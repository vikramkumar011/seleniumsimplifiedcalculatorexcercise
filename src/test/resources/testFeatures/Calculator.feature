@exercise
Feature: Selenium Simplified Calculator Exercise

  Background: Launch Web Page
    Given Open the simplified calculator page

  @plus
  Scenario Outline: Validate operations on plus operator
    When enter "<number1>" on first number input
    And select "<operator>" to perform operation
    And enter "<number2>" on second number input
    Then validate "<operator>" is working correctly for "<number1>" and "<number2>"
    Examples:
      | number1 | operator | number2 |
      | 12      | plus     | 6       |
      | -12     | plus     | -6      |
      | 12      | plus     | 0       |
      | 12      | plus     | 6.0     |
      | 12.0    | plus     | -6.0    |
      | 2       | plus     | 6       |

  @minus
  Scenario Outline: Validate operations on minus operator
    When enter "<number1>" on first number input
    And select "<operator>" to perform operation
    And enter "<number2>" on second number input
    Then validate "<operator>" is working correctly for "<number1>" and "<number2>"
    Examples:
      | number1 | operator | number2 |
      | 12      | minus    | 6       |
      | -12     | minus    | -6      |
      | 0       | minus    | 12      |
      | 12      | minus    | 6.0     |
      | 12.0    | minus    | -6.0    |
      | 2       | minus    | 6       |

  @times
  Scenario Outline: Validate operations on multiply operator
    When enter "<number1>" on first number input
    And select "<operator>" to perform operation
    And enter "<number2>" on second number input
    Then validate "<operator>" is working correctly for "<number1>" and "<number2>"
    Examples:
      | number1 | operator | number2 |
      | 12      | times    | 6       |
      | -12     | times    | -6      |
      | 12      | times    | 0       |
      | 12      | times    | 6.0     |
      | 12.0    | times    | -6.0    |
      | 2       | times    | 6       |

  @divide
  Scenario Outline: Validate operations on divide operator
    When enter "<number1>" on first number input
    And select "<operator>" to perform operation
    And enter "<number2>" on second number input
    Then validate "<operator>" is working correctly for "<number1>" and "<number2>"
    Examples:
      | number1 | operator | number2 |
      | 12      | divide   | 6       |
      | -12     | divide   | -6      |
      | 0       | divide   | 12      |
      | 12      | divide   | 0       |
      | 12      | divide   | 6.0     |
      | 12.0    | divide   | -6.0    |
      | 2       | divide   | 6       |

  @char
  Scenario Outline: Validate operations on characters
    When enter "<number1>" on first number input
    And select "<operator>" to perform operation
    And enter "<number2>" on second number input
    Then validate "<operator>" is working correctly for "<number1>" and "<number2>"
    Examples:
      | number1 | operator | number2 |
      | abc     | plus     | 6       |
      | abc     | minus    | -6      |
      | abc     | times    | 12      |
      | abc     | divide   | 0       |
      | abc     | divide   | abc     |




