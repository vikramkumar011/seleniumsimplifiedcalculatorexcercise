package com.selenium.web.managers;

import com.selenium.web.pageObjects.CalculatorPage;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {
    private WebDriver driver;
    private CalculatorPage calculatorPage;

    public PageObjectManager(WebDriver driver){
        this.driver = driver;
    }

    public CalculatorPage getCalculatorPage(){
        return (calculatorPage == null)? calculatorPage = new CalculatorPage(driver) : calculatorPage;
    }

}
