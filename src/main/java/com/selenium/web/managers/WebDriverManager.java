package com.selenium.web.managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class WebDriverManager {
    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
    private static DriverType driverType;
    private WebDriver driver;

    public enum DriverType{
        CHROME
    }

    public WebDriverManager(){
        driverType = FileReaderManager.getInstance().getConfigFileReader().getBrowser();
    }

    public static String getChromeDriverProperty( ){
        return CHROME_DRIVER_PROPERTY;
    }

    private WebDriver createDriver(){
        switch (driverType){
            case CHROME:
                System.setProperty(getChromeDriverProperty(), FileReaderManager.getInstance().getConfigFileReader().getChromeDriverPath());
                driver  = new ChromeDriver();
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                driver.manage().window().maximize();
        }
        return driver;
    }

    public WebDriver getDriver(){
        if(driver==null) {
            driver = createDriver();
        }
        return driver;
    }

    public void closeDriver() throws IOException {
        try{
            driver.close();
            driver.quit();
        }finally {
            Process process = null;
            if(driver.toString().equalsIgnoreCase("CHROME")){
                process = Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
            }
            process.destroy();
        }
    }

    public void launchWebPage(){
        driver.get(FileReaderManager.getInstance().getConfigFileReader().getWebPageUrl());
    }
}

