package com.selenium.web.pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CalculatorPage {
    WebDriver driver;
    public static final Logger log = LogManager.getLogger(CalculatorPage.class);

    @FindBy(id = "number1")
    protected WebElement inputNumber1;
    @FindBy(id = "number2")
    protected WebElement inputNumber2;
    @FindBy(id = "function")
    protected WebElement selectOperator;
    @FindBy(id = "calculate")
    protected WebElement buttonCalculate;
    @FindBy(id = "answer")
    protected WebElement textAnswer;

    public CalculatorPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterNumberOnFirstInput(String number1) {
        inputNumber1.sendKeys(number1);
        log.info("Entered Number 1 : "+number1);
    }

    public void enterNumberOnSecondInput(String number2) {
        inputNumber2.sendKeys(number2);
        log.info("Entered Number 2 : "+number2);
    }

    public void selectOperatorToPerformOperation(String operator) {
        Select operators = new org.openqa.selenium.support.ui.Select(selectOperator);
        operators.selectByValue(operator);
        log.info("Selected operator : "+operator);
    }

    public boolean validateOperation(String operator, String number1, String number2) {
        buttonCalculate.click();
        log.info("Clicked on calculate button");
        String actualAnswer = textAnswer.getText();
        float no1 = 0;
        float no2 = 0;
        try{
            no1 = Float.parseFloat(number1);
            no2 = Float.parseFloat(number2);
        }catch (NumberFormatException nfe){
            // Validating if input provided is character then it should result in Error
            Assert.assertEquals("Answer should be ERR", "ERR", actualAnswer);
            return true;
        }
        float expectedAnswer = 0;
        switch (operator){
            case "plus":
                expectedAnswer = no1+no2;
                //Validating plus operation
                Assert.assertEquals(expectedAnswer,Float.parseFloat(actualAnswer), 0.0f);
                break;
            case "minus":
                expectedAnswer = no1-no2;
                //Validating minus operation
                Assert.assertEquals(expectedAnswer,Float.parseFloat(actualAnswer), 0.0f);
                break;
            case "times":
                expectedAnswer = no1*no2;
                //Validating multiply operation
                Assert.assertEquals(expectedAnswer,Float.parseFloat(actualAnswer), 0.0f);
                break;
            case "divide":
                if(no2 == 0){
                    //Validating if divide by 0 it should throw Error
                    Assert.assertEquals("Answer Should be ERR","ERR",actualAnswer);
                }else{
                    expectedAnswer = no1/no2;
                    //Validating divide operation
                    Assert.assertEquals(expectedAnswer,Float.parseFloat(actualAnswer), 0.0f);
                }
                break;
            default:
                break;
        }
        return true;
    }
}
