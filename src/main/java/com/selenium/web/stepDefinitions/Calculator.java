package com.selenium.web.stepDefinitions;

import com.selenium.web.context.TestContext;
import com.selenium.web.pageObjects.CalculatorPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Calculator {
    TestContext testContext;
    private CalculatorPage calculatorPage;

    public Calculator(TestContext context){
        testContext = context;
        calculatorPage = testContext.getPageObjectManager().getCalculatorPage();
    }

    @Given("^Open the simplified calculator page$")
    public void openTheSimplifiedCalculatorPage() {
        testContext.getWebDriverManager().launchWebPage();
    }

    @When("^enter \"([^\"]*)\" on first number input$")
    public void enterOnFirstNumberInput(String number1) {
        calculatorPage.enterNumberOnFirstInput(number1);
    }

    @And("^select \"([^\"]*)\" to perform operation$")
    public void selectToPerformOperation(String operator) {
        calculatorPage.selectOperatorToPerformOperation(operator);
    }

    @And("^enter \"([^\"]*)\" on second number input$")
    public void enterOnSecondNumberInput(String number2) {
        calculatorPage.enterNumberOnSecondInput(number2);
    }

    @Then("^validate \"([^\"]*)\" is working correctly for \"([^\"]*)\" and \"([^\"]*)\"$")
    public void validateIsWorkingCorrectly(String operator,String number1,String number2) {
        calculatorPage.validateOperation(operator, number1, number2);
    }
}
