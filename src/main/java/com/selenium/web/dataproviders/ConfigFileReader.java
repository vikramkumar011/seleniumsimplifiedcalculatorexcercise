package com.selenium.web.dataproviders;

import com.selenium.web.managers.WebDriverManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
    private Properties properties;
    private final String propertyFilePath = "src/main/java/com/selenium/web/config/configuration.properties";

    public ConfigFileReader(){
        BufferedReader reader;
        try{
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try{
                properties.load(reader);
            }catch (IOException e){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
            throw new RuntimeException("Configuration properties not found at "+propertyFilePath);
        }
    }

    public String getChromeDriverPath(){
        String driverPath = properties.getProperty("driverPath_CHROME");
        if(driverPath!=null) return System.getProperty("user.dir")+driverPath;
        else throw new RuntimeException("driverPath not specified in the configuration.properties");
    }

    public WebDriverManager.DriverType getBrowser(){
        String browserNAME = properties.getProperty("BROWSER");
        if(browserNAME == null || browserNAME.equalsIgnoreCase("CHROME")){
            return WebDriverManager.DriverType.CHROME;
        }
        else throw new RuntimeException("Browser not specified in the configuration.properties");
    }

    public String getWebPageUrl(){
        String url = properties.getProperty("URL");
        if(url != null) return  url;
        else throw new RuntimeException("URL not specified in the configuration.properties");
    }
}
