### selenium-cucumber-java-simplified-calculator-excercise

### Installation (pre-requisites)

1. JDK 1.8+ (make sure Java class path is set)
2. Maven (make sure .m2 class path is set)
3. IntelliJ IDEA
4. Intellij Plugins for
    - Maven
    - Cucumber
5. Browser driver (make sure you have your desired browser driver and class path is set)

### Framework set up

Fork / Clone repository from https://gitlab.com/vikramkumar011/seleniumsimplifiedcalculatorexcercise.git or download zip and set
it up in your local workspace.

### Run Some Sample Tests

Open terminal (MAC OSX) or command prompt / power shell (for windows OS) and navigate to the project directory
type `mvn clean test` command to run features. With this command it will invoke the default Chrome browser and will
execute the tests.

Please note that browser drivers are not included as part of this framework. The reason for not including is that
selenium browser driver version are varies based on the browser version that you are using and also selenium server
version.
